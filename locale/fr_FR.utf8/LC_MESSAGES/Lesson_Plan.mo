��            )         �  ;   �  
   �     �       (   "     K     S  2   h  6   �  
   �     �      �          +     :     H     T     a     v          �     �  l   �  &     *   ?     j  r   o  >   �  _   !     �  �  �  =   v     �     �  $   �  -   �     -	     6	  3   L	  Y   �	     �	     �	  %    
  '   &
     N
     ]
     o
     }
     �
     �
     �
     �
     �
  s   �
  /   I  /   y     �  �   �  ?   C  v   �     �                                         	   
                                                                                            A lesson already exists for this course period and date: %s Add Lesson Add new Part Content and teacher activity Do you want to add a lesson on that day? Entries Formative assessment How are you explaining and illustrating the topic? How do you plan to assess learning as it is happening? Last Entry Learner activity Learning materials and resources Learning outcomes/objective Lesson Lessons Lesson Number Lesson Plan Lesson Plans Lesson and its Parts Location Part of the Lesson Parts of the Lesson Read The Course Period is not scheduled on that date: %s. You can correct the date in case of mistake, or cancel. The lesson has been added to the plan. The lesson has been removed from the plan. Time What are learners expected to learn after completing the lesson? These should be specific and able to be assessed. What are the learners doing to help them understand the topic? What resources will you use that will support the teaching, learning and assessment activities? min. Project-Id-Version: Lesson Plan module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-01-13 17:43+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2,3
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Une leçon existe déjà pour cette classe et cette date : %s Ajouter une leçon Ajouter une partie Contenu et activité de l'enseignant Voulez-vous ajouter une leçon pour ce jour ? Entrées Évaluation formative Comment expliquez-vous et illustrez-vous le sujet ? Comment prévoyez-vous d'évaluer l'apprentissage au fur et à mesure qu'il se déroule ? Dernière entrée Activité apprenant Matériel et ressources pédagogiques Résultats de l'apprentissage/objectifs Leçon Leçons Numéro de leçon Plan de cours Plans de cours Leçon et ses parties Lieu Partie de la leçon Parties de la leçon Lire La classe n'est pas programmée pour cette date: %s. Vous pouvez corriger la date en cas d'erreur, ou bien annuler. Cette leçon a été ajoutée au plan de cours. Cette leçon a été retirée du plan de cours. Heure Qu'est-ce que les apprenants sont censés apprendre à l'issue de la leçon ? Ces éléments doivent être spécifiques et pouvoir être évalués. Que font les apprenants pour les aider à comprendre le sujet ? Quelles ressources allez-vous utiliser pour soutenir les activités d'enseignement, d'apprentissage et d'évaluation ? min. 